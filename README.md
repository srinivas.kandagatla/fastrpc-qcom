This document gives quick instructions and status on simple testing of FastRPC on various Qualcomm SoC with upstream linux kernel

## Test Instructions

https://git.codelinaro.org/linaro/qcomlt/fastrpc/-/wikis/Testing-FastRPC

# FastRPC Status
| SoC      | DSP      | Kernel | get_serial | calculator |
| -------- | -------- |------- | ---------  | -----------|
| sdm845   | ADSP     |        |            |            |        
|          | CSSP     |        |            |            |
|          | SDSP     |        |            |            |
| -------- | -------- |------- | ---------  | -----------|
| sm8250   | ADSP     |        |            |            |        
|          | CSSP     |        |            |            |
|          | SDSP     |        |            |            |
| -------- | -------- |------- | ---------  | -----------|
| sm8450   | ADSP     |        |            |            |        
|          | CSSP     |        |            |            |
|          | SDSP     |        |            |            |
| -------- | -------- |------- | ---------  | -----------|
| sc8280xp | ADSP     |        |            |            |        
|          | CSSP     |        |            |            |
|          | SDSP     |        |            |            |
| -------- | -------- |------- | ---------  | -----------|
| sm8550   | ADSP     |        |            |            |        
|          | CSSP     |        |            |            |
|          | SDSP     |        |            |            |
| -------- | -------- |------- | ---------  | -----------|
| sm8650   | ADSP     |        |            |            |        
|          | CSSP     |        |            |            |
|          | SDSP     |        |            |            |
| -------- | -------- |------- | ---------  | -----------|


# Instructions to test get_serial
##SDM845 
<TODO>

# Instructions to test calculator
##SDM845 
<TODO>
